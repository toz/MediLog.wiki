I will add small snippets here whenever I get to it, over time this hopefully turns into a proper user manual

[Data entry](Data-Entry)

[Import/Export](Import-Export)

[User feedback](User-Feedback)

[Text templates](Text-templates)

[Installing Beta versions](Installing-Beta-versions)

[Collecting Crash Logs](https://codeberg.org/toz/MediLog/wiki/Crash-logs)

[Multiple measurements](https://codeberg.org/toz/MediLog/wiki/Multiple-measurements)

[Settings](https://codeberg.org/toz/MediLog/wiki/Settings)

[Troubleshooting](https://codeberg.org/toz/MediLog/wiki/Troubleshooting)

[User Feedback](https://codeberg.org/toz/MediLog/wiki/User-Feedback)
