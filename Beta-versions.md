The latest beta can be found here: https://cloud.zell-mbc.com/s/medilogDownload

**Make sure you got a backup of your data before moving any further!**

Unfortunately Android does not allow you to run multiple versions of the same app in parallel.
Therefore you need to either work around this limitation (1) with additional tools, or upgrade/reinstall (2) MediLog.

### 1. Run in parallel
If you want to keep your existing installation you might use tools like https://appcloner.app/ to create a second instance of MediLog.
Once you got the second instance running you can start testing with either an empty database or you export your data from the released app and import into the beta build.

### 2. Upgrade
If you don't want to install additional tools what needs to be done to test a beta version of Medilog differs depending on where you installed your current version from. 

a, If you installed from MediLog F-Droid or compiled yourself, you will have to remove the current application and do a new install of the beta. Important: removing means you'll lose all data!! If you are using your "production" installation make sure you create  a backup beforehand!
This is because the F-Droid developer key (and your own if you compiled MediLog yourself) is different than my developer key, and Android does not allow different keys for the same application.

b, If you installed from Codeberg, IzziOnDroid or any other repository which offers MediLog signed with my developer key, you can simply update the APK and effectively overwrite your current install.

Therefore:

**If you installed MediLog directly from Codeberg/IzzyOnDroid/Obtanium/etc.:**
  1. Create a backup
  2. Install the beta apk
  3. Test
     - Try to break whatever's new
     - Check if the screens are intuitive
     - Any language issues?
     - Etc.

  If things work, you can stay on the beta and upgrade again once the final release becomes available 

**If you installed MediLog from F-Droid or compiled it yourself:**
  1. Create a backup
  2. Remove MediLog
  3. Install the beta apk
  4. Import the backup (a successful import is already an important test for each new beta!)
  5. Test
     - Try to break whatever's new
     - Check if the screens are intuitive
     - Any language issues?
     - Etc.
  
You can provide feedback via the usual channels: 
- https://codeberg.org/toz/MediLog/issues (Preferred!)
- Fediverse (@thomas@zell-mbc.com)
- Matrix (@medilog:matrix.com) 
- Email (medilog@zell-mbc.com)
