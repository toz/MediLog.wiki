As of version 3.1.0 Medilog offers a Document tab which is supposed to hold documents one wants to have readily available such as medication plans and medical reports.
Documents can either be loaded from within MediLog or sent to MediLog from 3rd party apps.
At present only PDF and image documents are supported-




