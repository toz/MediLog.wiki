Starting with version 3.0 MediLog introduced the ability to collect crash logs. Crash logs get created in case of a fatal error which cause the app to crash.
I know crash logs are a controversial subject for security minded individuals, so I want to explain how this feature has been implemented, what exactly is happening and why I think the way I implemented it is acceptable.

### What technology is used?
The library used to collect crash logs is https://www.acra.ch/, it's FOSS and is used in many other FOSS projects such as the F-Droid Android client.
There are several ways to implement crash logging with Acra, the way MediLog has chosen is without a back-end but rather email only.

### When do logs get collected?
Ideally never! Realistically, whenever the app crashes. This should be very rare though. As the developer I have a strong interest in not creating crashes!

However, crashes do occur, and when they do, troubleshooting is extremely difficult without visibility into what happened on a users device.

This is where crash logs come into play. Android by default allows to collect very comprehensive crash logs, covering everything which happened on a device across all applications. And there are means to send it off to the developers. However, because these logs tend to be very verbose it's likely some personal data gets collected as well, which I don't need and don't want.

Arca on the other hand creates very targeted crash logs which only cover the things related to MediLog and to the crash at hand. This is better for you because less data is collected, and much better for me because I don't need to sift through 100 MB of text.

### What does the process look like?

1. Crash occurs, which will trigger Acra

2. The dialog below pops up.
Here you got the first opportunity to say "no" to sending a crash log, or even better, give me some insight into what you did before the crash occurred:


![Crash Log Dialog](/uploads/CrashLogs/CrashDialog.png)

3. After you pressed ok your local email client will open up. Include some friendly words, maybe add yourself as additional recipient and send off the email:

![Crash log Email](/uploads/CrashLogs/CrashEmail.png)


As can be seen from the attachment size in the email screenshot, the crash log is only a small section of what a regular Android Crash log would cover. A lot less likely to catch any personal data that way.
Here's an example of a crash log:

![Sample Crash Log](/uploads/CrashLogs/SampleCrashLog.txt)


### Can you disable Crash logs?

If you don't like this feature at all, you are able to "turn off" crash logs and you will never be bothered with the dialog above. But please consider that in case a crash happens during the startup of the app there won't be a chance to turn this back on. And if I can't figure out what's wrong some other way this may mean there is no way to get access to your data again. In short, if you decide to switch crash logs off, make sure your backups are working.

![Crash Log Setting](/uploads/CrashLogs/CrashSetting.png)
