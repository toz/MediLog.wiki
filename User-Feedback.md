Over time there were requests for lots of new tabs, Blood-sugar, Water consumption, Temperature and many more values people have requested and I have happily added to help. 

But unfortunately I often never hear back after these tabs were implemented, which makes me think that at the end they don't get used. But nevertheless each tab continues to add quite a bit of work for me.

Hence I added a **opt-in** user feedback feature which will send the list of active tabs whenever the MediLog is started.
A possibility for you to tell me which tabs you are using, and what's in use I will keep.

Navigate to the settings menu to see the current state:

![image](uploads/4cde0cca1d6a5644c9b5e8a4a6114916/image.png)

I want to be most transparent about what's being sent, so the screen below shows when feedback was sent last and what was sent:

![image](uploads/24de1a0058a8e2cc8da5fbb81737e4a0/image.png)

The long ID is a random identifier created by the Android randomUUID() function. Nothing personal or identifying you goes into this number, I need some sort of unique identifier though so I don't count your device multiple times.
The second string reflects the tabs in use, the actual value is the number of records in a tab. A value > 0 means a tab is used, last number represents the number of attachments which have been added to the Medilog database.

There's also the possibility to send me one off feedback via the Instant Feedback button or, of course, via email or GitLab.