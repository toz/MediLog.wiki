Tags serve different purpose and as of now are not available in all tabs.
You can create an unlimited number of tags, although from a practical point of view using too many may make things complicated. Each tag can be linked to a colour to visually separate them in their respective tabs. Each record can be associated with multiple tags. If tags are used, records without a tag are treated like an undefined tag.

## Diary Tab
TBD

## Fluid Tab

Tags allow to track multiple values in parallel. Values which are different but somehow relate to each other.

One use case is the need to track liquid intake and urine output. Two distinct values but logically connected. Defining one or two tags and assigning recordings to each allows to capture both values in the same tab, but keeping them separate when it comes to reporting.
