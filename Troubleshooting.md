### Test data
In cases where it makes sense to send me real data to help my troubleshooting, please use this secure file drop instead of unprotected email: https://cloud.zell-mbc.com/s/medilogUpload

### Beta/Debug versions
In case a debug or beta version of MediLog is required I will provide it here: https://cloud.zell-mbc.com/s/medilogDownload

### Crash logs
In case your app is crashing I might ask you for a crash log. There are several ways to extract the app crash data from an Android device. However, the easiest way is usually via the Android 
Bug Report Feature:

1. Go to  Android device Settings and enable Developer Options. (https://developer.android.com/studio/debug/dev-options)
2. Go to the System option and click on About Your Phone. Tap seven times on Build Number.
3. Go back to Developer Options and click on Take Bug Report.
4. Once the bug report is ready upload it to my secure file drop https://cloud.zell-mbc.com/s/medilog

What would be helpful is if you could also tell at what time the crash occurred, after all a typical crash log is > 20MB of data I need to sift through…
