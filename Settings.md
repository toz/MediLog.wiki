Below is some more background on settings which created questions:

## Blood Pressure

### Thresholds
MediLog follows the guidance of the European Society of Hypertension as stated in this Wikipedia article https://en.wikipedia.org/wiki/Blood_pressure
Following means MediLog implements the 3 grades of Hypertension and uses the ESH threshold values as default values.
The grades are reflected in the application like below:

_Blood Pressure tab_
- Grade 1: If a blood pressure value is higher than what's specified in Grade 1, text will be shown in amber
- Grade 2: If a blood pressure value is higher than what's specified in Grade 2, text will be shown in red
- Grade 3: If a blood pressure value is higher than what's specified in Grade 3, text will be shown in red and bold

_Blood Pressure PDF_
- Grade 1: If a blood pressure value is higher than what's specified in Grade 1, text will be shown in bold
- Grade 2: If a blood pressure value is higher than what's specified in Grade 2, text will be shown in bold and underlined
- Grade 3: If a blood pressure value is higher than what's specified in Grade 3, text will be shown in bold and underlined

_Blood Pressure chart_

- If the thresholds are enabled in settings, they will show as simple lines at their respective value

If your country follows the guidance of the ESH you will likely never have to change these settings.
If your country recommends different levels please adapt the values accordingly.

If ever the ESH recommendation changes I will change the default values, for existing MediLog installs these values will have to be manually changed here.