## General

MediLog supports two types of data entry. Edit and Quick.

![image](uploads/cb5a0eaa03bf01a64fc140d36e6683f0/image.png)


1. Quick is the default setting and makes most sense when you capture your readings as you do the measurements.
Enter your data (1), hit + (2) and you are done.
MediLog will automatically add the current date and time to this record.

![image](uploads/37b9b7b54028e2df48d13799b8b4ffea/image.png) 


2. Edit (not quick) is for cases where you might add multiple recordings in one batch and hence need to change date and time of the record you enter.
In this case there won't be any entry fields on the main screen. And the plus button will open the "data edit" view which allows to adjust date and time when you enter your data.

![image](uploads/1afac2999d4d526b7d6db7e4236934b4/image.png) 

## Tabs
Data entry should be self explaining, however there are a few subtleties which might need explanation.

### Blood Pressure Tab
Some blood pressure measuring appliances report heart rhythm issues as well. This usually is a binary value, good/bad, which can be captured in the blood pressure edit view.
Select the heart item to capture this value.

![image](uploads/fc0f00b40880d3042e67da9b8818aab8/image.png)

### Diary Tab
The diary tab allows you to capture whatever you think is noteworthy. It allows to also capture a general state good, not so good, bad, or whatever label you want to give it. It's basically a 3 level traffic light.
Hit the dot to change the colour.

![image](uploads/59ea31a6d32c84094ab5edd34f658cc7/image.png)