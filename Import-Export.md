## Import/Restore

Data needs to be in CSV format and follow the below structure:

```
timestamp, comment, type, value1, value2, value3, value4, attachment
2020-12-07 07:42:25,"",1,99.6,,,,
```

A header line, with column headers separated by a delimiter. MediLog accepts , ; and | as delimiters.
The header line in the example here would be "timestamp, comment, type, value1, value2, value3, value4, attachment".
Data items are associated to their tabs by the type value:
```
WEIGHT = 1
BLOODPRESSURE = 2
DIARY = 3
WATER = 4
GLUCOSE = 5
TEMPERATURE = 6
OXIMETRY = 7
```

The data line in the example above shows a weight entry.

If you plan to import data from another application I recommend you add a dummy entry to MediLog, export, and modify your data so it fits the MediLog export.

## Export/Backup
There are two types backups, individual files and ZIP Backup. You can change the type via settings/ZIP Backup
1. Individual files (Not secure)

First you need to specify the folder in which you want to store the backup. Downloads is a good option.
Second, MediLog creates a CSV file for each data table / logged item


2. ZIP Backup (secure if password is used)

MediLog will first ask you for a password before the ZIP file is created, you can store a default password in the settings/ZIP Password and you won't get asked again. If you don't specify a password you will create an unprotected ZIP file. 
Second you need to select the export location
Third, MediLog will create a protected ZIP file with the individual CSV files.
 